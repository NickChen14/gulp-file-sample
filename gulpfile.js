var gulp			= require('gulp'),
	browserSync 	= require('browser-sync').create(),
	gulpIf 			= require('gulp-if'),
	sass 			= require('gulp-sass'),
	autoprefixer 	= require('gulp-autoprefixer'),
	concat			= require('gulp-concat'),
	minifyCss 		= require('gulp-minify-css'),
	uglify			= require('gulp-uglify'),
	rename			= require('gulp-rename'),
	sourcemaps 		= require('gulp-sourcemaps'),
	plumber 		= require('gulp-plumber'),
	gutil 			= require('gulp-util'),
    connect 		= require('gulp-connect-php'),
    env 			= require('gulp-env'),
    consolidate 	= require("gulp-consolidate");

var reload = browserSync.reload;

//environment variables
env({
	file: ".env.json"
});

var src = 'source',
	dest = 'public';

var env = {
    isProduction: false,
    autoprefixer: ['last 2 version']
}

//serve php files
gulp.task('php', function() {
	connect.server({
		base: dest,
		port: process.env.PHPPORT
	});
});

gulp.task('serve', ['php'], function() {
    browserSync.init({
        proxy: process.env.PROXY,
        port: process.env.PORT,
        notify: false,
        open: false
    });

    gulp.watch(src + '/sass/**/*.scss', ['make:css']);
    gulp.watch(src + '/js/**/*js', ['make:app-js', 'make:js']);
    gulp.watch(src + '/js/**/*.js').on('change', reload);
    gulp.watch(dest + '/**/*.php').on('change', reload);
});

gulp.task('make:css', function() {
	return gulp.src(src + '/sass/**/*.scss')
	    .pipe(plumber(function(error) {
	    	gutil.beep();
            gutil.log(gutil.colors.red(error.message));
            this.emit('end');
        }))
		.pipe(gulpIf(!env.isProduction, sourcemaps.init()))
		.pipe(sass())
		.pipe(autoprefixer(env.autoprefixer))
		.pipe(gulpIf(!env.isProduction, sourcemaps.write()))
		.pipe(gulpIf(env.isProduction, minifyCss()))
		.pipe(gulp.dest(dest + '/css'))
		.pipe(browserSync.stream());
});

gulp.task('make:app-js', function() {
    return gulp.src([
        src + '/js/app.js',
        src + '/js/controllers/*.js',
        src + '/js/directives/*.js'
    ])
    .pipe(concat('all-app.js'))
    .pipe(gulp.dest(dest + '/js'));
});

gulp.task('make:js', function() {
    return gulp.src([
        src + '/js/main.js'
    ])
    .pipe(concat('main.min.js'))
    .pipe(gulpIf(env.isProduction, uglify()))
    .pipe(gulp.dest(dest + '/js'));
})

//run gulp
gulp.task('default', ['make:css', 'make:app-js', 'make:js', 'serve']);